package hr.tvz.pios.bazapodataka;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Uloga;
import hr.tvz.pios.model.Valuta;

public class Baza {
	
	/** Konekcija prema bazi podataka. */
	private Connection c = null;

	/**
	 * Konstruktor koji otvara konekciju na bazu podataka.
	 */
	public Baza() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:bazaPIOS.db");
		} catch ( Exception e ) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}
	
	public void zatvoriBazu() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void dodajPrihod(Prihod prihod) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("INSERT INTO tPrihodi (KorisnikId, ValutaId, TipPrihodaId, BrutoIznos, NetoIznos, Datum) " +
					"VALUES (?, ?, ?, ?, ?, ?);");
			stmt.setInt(1, prihod.getKorisnik().getId());
			stmt.setInt(2, prihod.getValuta().getId());
			stmt.setInt(3, prihod.getTip().getId());
			stmt.setDouble(4, prihod.getBrutoIznos());
			stmt.setDouble(5, prihod.getNetoIznos());
			stmt.setDate(6, new Date(prihod.getDatum().getTime()));
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}	
	}

	public Valuta dohvatiDefaultnuValutu() {
		Valuta valuta = null;
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM sValute where Id = 1  ORDER BY Id");
			rs.next();
			valuta = new Valuta(rs.getInt("Id"), rs.getString("Kratica"));
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return valuta;
	}

	public List<Valuta> dohvatiPreostaleValute() {
		List<Valuta> valute = new ArrayList<>();
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM sValute where Id != 1 ORDER BY Id");
			while (rs.next()) {
				Valuta valuta = new Valuta(rs.getInt("Id"), rs.getString("Kratica"));
				valute.add(valuta);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		
		return valute;
	}

	public List<TipPrihoda> dohvatiSveTipovePrihoda() {
		List<TipPrihoda> tipoviPrihoda = new ArrayList<>();
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM sTipoviPrihoda ORDER BY Id");
			while (rs.next()) {
				TipPrihoda tipPrihoda = new TipPrihoda(rs.getInt("Id"), rs.getString("Opis"));
				tipoviPrihoda.add(tipPrihoda);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return tipoviPrihoda;
	}

	public List<TipRashoda> dohvatiSveTipoveRashoda() {
		List<TipRashoda> tipoviRashoda = new ArrayList<>();
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM sTipoviRashoda ORDER BY Id");
			while (rs.next()) {
				TipRashoda tipRashoda = new TipRashoda(rs.getInt("Id"), rs.getString("Opis"));
				tipoviRashoda.add(tipRashoda);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return tipoviRashoda;
	}

	public void dodajRashod(Rashod rashod) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("INSERT INTO tRashodi (KorisnikId, Iznos, ValutaId, TipRashodaId,"
					+ " Datum, PlaniraniRashod) " + "VALUES (?, ?, ?, ?, ?, ?);");
			stmt.setInt(1, rashod.getKorisnik().getId());
			stmt.setDouble(2, rashod.getIznos());
			stmt.setInt(3, rashod.getValuta().getId());
			stmt.setInt(4, rashod.getTip().getId());
			stmt.setDate(5, new Date(rashod.getDatum().getTime()));
			stmt.setBoolean(6, rashod.isPlaniraniRashod());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}	
		
	}
	
	public List<Prihod> dohvatiSvePrihode(Korisnik korisnik) {
		List<Prihod> prihodi = new ArrayList<>();
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tPrihodi.*, sValute.Kratica, sTipoviPrihoda.Opis FROM tPrihodi "
					+ "JOIN sValute ON tPrihodi.ValutaId=sValute.Id JOIN sTipoviPrihoda on "
					+ "tPrihodi.TipPrihodaId=sTipoviPrihoda.Id WHERE KorisnikId=? ORDER BY Id");
			stmt.setInt(1, korisnik.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				java.util.Date datum = new Date(Long.parseLong(rs.getString("Datum")));
				Prihod prihod = new Prihod(rs.getInt("Id"), korisnik, new Valuta(rs.getInt("ValutaId"), rs.getString("Kratica")),
						new TipPrihoda(rs.getInt("TipPrihodaId"), rs.getString("Opis")), rs.getDouble("BrutoIznos"),
						rs.getDouble("NetoIznos"), datum);
				prihodi.add(prihod);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return prihodi;
	}

	public List<Rashod> dohvatiSveRashode(Korisnik korisnik) {
		List<Rashod> rashodi = new ArrayList<>();
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tRashodi.*, sValute.Kratica, sTipoviRashoda.Opis FROM tRashodi "
					+ "JOIN sValute ON tRashodi.ValutaId=sValute.Id JOIN sTipoviRashoda on "
					+ "tRashodi.TipRashodaId=sTipoviRashoda.Id WHERE KorisnikId=? ORDER BY Id");
			stmt.setInt(1, korisnik.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				java.util.Date datum = new Date(Long.parseLong(rs.getString("Datum")));
				Rashod rashod = new Rashod(rs.getInt("Id"), korisnik, rs.getDouble("Iznos"),new Valuta(rs.getInt("ValutaId"),
						rs.getString("Kratica")), new TipRashoda(rs.getInt("TipRashodaId"), rs.getString("Opis")), datum, rs.getBoolean("PlaniraniRashod"));
				rashodi.add(rashod);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return rashodi;
	}

	public double dohvatiStanjeRacuna(Korisnik korisnik) {
		double stanjeRacuna = 0;
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT StanjeRacuna FROM tKorisnici WHERE Id=?");
			stmt.setInt(1, korisnik.getId());
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				stanjeRacuna = rs.getDouble("StanjeRacuna");
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return stanjeRacuna;
	}

	public void azurirajPrihod(Prihod prihod) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("UPDATE tPrihodi SET KorisnikId=?, ValutaId=?, TipPrihodaId=?, BrutoIznos=?,"
					+ " NetoIznos=?, Datum=?" + " WHERE Id=?;"); 
			stmt.setInt(1, prihod.getKorisnik().getId());
			stmt.setInt(2, prihod.getValuta().getId());
			stmt.setInt(3, prihod.getTip().getId());
			stmt.setDouble(4, prihod.getBrutoIznos());
			stmt.setDouble(5, prihod.getNetoIznos());
			stmt.setDate(6, new Date(prihod.getDatum().getTime()));
			stmt.setInt(7, prihod.getId());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}		
		
	}

	public void azurirajRashod(Rashod rashod) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("UPDATE tRashodi SET KorisnikId=?, Iznos=?, ValutaId=?, TipRashodaId=?,"
					+ " Datum=?, PlaniraniRashod=?" + " WHERE Id=?;"); 
			stmt.setInt(1, rashod.getKorisnik().getId());
			stmt.setDouble(2, rashod.getIznos());
			stmt.setInt(3, rashod.getValuta().getId());
			stmt.setInt(4, rashod.getTip().getId());
			stmt.setDate(5, new Date(rashod.getDatum().getTime()));
			stmt.setBoolean(6, rashod.getPlaniraniRashod());
			stmt.setInt(7, rashod.getId());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}		
		
	}

	public void azurirajStanjeRacuna(Korisnik korisnik) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("UPDATE tKorisnici SET StanjeRacuna=?" + " WHERE Id=?;"); 
			stmt.setDouble(1, korisnik.getStanjeRacuna());
			stmt.setInt(2, korisnik.getId());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}		
		
	}

	public Prihod dohvatiPrihod(Korisnik korisnik, int id) {
		Prihod prihod = null;
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tPrihodi.*, sValute.Kratica, sTipoviPrihoda.Opis FROM tPrihodi "
					+ "JOIN sValute ON tPrihodi.ValutaId=sValute.Id JOIN sTipoviPrihoda on "
					+ "tPrihodi.TipPrihodaId=sTipoviPrihoda.Id WHERE tPrihodi.Id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				java.util.Date datum = new Date(Long.parseLong(rs.getString("Datum")));
				prihod = new Prihod(rs.getInt("Id"), korisnik, new Valuta(rs.getInt("ValutaId"), rs.getString("Kratica")),
						new TipPrihoda(rs.getInt("TipPrihodaId"), rs.getString("Opis")), rs.getDouble("BrutoIznos"),
						rs.getDouble("NetoIznos"), datum);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return prihod;
		
	}

	public Rashod dohvatiRashod(Korisnik korisnik, int id) {
		Rashod rashod = null;
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tRashodi.*, sValute.Kratica, sTipoviRashoda.Opis FROM tRashodi "
					+ "JOIN sValute ON tRashodi.ValutaId=sValute.Id JOIN sTipoviRashoda on "
					+ "tRashodi.TipRashodaId=sTipoviRashoda.Id WHERE tRashodi.Id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				java.util.Date datum = new Date(Long.parseLong(rs.getString("Datum")));
				rashod = new Rashod(rs.getInt("Id"), korisnik, rs.getDouble("Iznos"),new Valuta(rs.getInt("ValutaId"),
						rs.getString("Kratica")), new TipRashoda(rs.getInt("TipRashodaId"), rs.getString("Opis")), 
						datum, rs.getBoolean("PlaniraniRashod"));
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return rashod;
	}

	public Connection dohvatiKonekciju() {
		// TODO Auto-generated method stub
		return c;
	}

	public Korisnik dohvatiKorisnika(String korisnickoIme) {
		Korisnik korisnik = null;
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tKorisnici.Id AS kId, tKorisnici.*, sUloge.Id AS UId, "
					+ "sUloge.Naziv AS UNaziv FROM tKorisnici JOIN sUloge ON tKorisnici.UlogaId=sUloge.Id "
					+ "WHERE KorisnickoIme=?");
			stmt.setString(1, korisnickoIme);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				korisnik = new Korisnik(rs.getInt("kId"), rs.getString("Ime"), rs.getString("Prezime"),
						new Uloga(rs.getInt("UId"), rs.getString("Unaziv")), rs.getDouble("StanjeRacuna"), 
						rs.getString("KorisnickoIme"), rs.getString("Lozinka"), rs.getInt("Odobren"));
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return korisnik;
	}

	public void spremiKorisnika(Korisnik korisnik) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("INSERT INTO tKorisnici (Ime, Prezime, KorisnickoIme,"
					+ " Lozinka, Odobren, UlogaId, StanjeRacuna) VALUES (?, ?, ?, ?, ?, ?, ?);");
			stmt.setString(1, korisnik.getIme());
			stmt.setString(2, korisnik.getPrezime());
			stmt.setString(3, korisnik.getKorisnickoIme());
			stmt.setString(4, korisnik.getLozinka());
			stmt.setInt(5, korisnik.getOdobren());
			stmt.setInt(6, korisnik.getUloga().getId());
			stmt.setDouble(7, korisnik.getStanjeRacuna());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}	
	}

	public void odobriKorisnika(Korisnik korisnik) {
		try {
			c.setAutoCommit(false);
			PreparedStatement stmt = c.prepareStatement("UPDATE tKorisnici SET Odobren=1" + " WHERE Id=?;"); 
			stmt.setInt(1, korisnik.getId());
			stmt.executeUpdate();
			stmt.close();
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}			
	}

	public List<Korisnik> dohvatiSveNeodobrene() {
		List<Korisnik> korisnici = new ArrayList<>();
		try {
			PreparedStatement stmt = c.prepareStatement("SELECT tKorisnici.Id AS kId, tKorisnici.*, sUloge.Id AS UId, "
					+ "sUloge.Naziv AS UNaziv FROM tKorisnici JOIN sUloge ON tKorisnici.UlogaId=sUloge.Id "
					+ "WHERE Odobren=0 ORDER BY Id");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Korisnik korisnik = new Korisnik(rs.getInt("kId"), rs.getString("Ime"), rs.getString("Prezime"),
						new Uloga(rs.getInt("UId"), rs.getString("Unaziv")), rs.getDouble("StanjeRacuna"), 
						rs.getString("KorisnickoIme"), rs.getString("Lozinka"), rs.getInt("Odobren"));
				korisnici.add(korisnik);
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);	
		}
		return korisnici;
	}

}
