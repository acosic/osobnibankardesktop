package hr.tvz.pios.model;

public class Korisnik {
	
	private int id;
	private String ime;
	private String prezime;
	private Uloga uloga;
	private double stanjeRacuna;
	private String korisnickoIme;
	private String lozinka;
	private int odobren;
	
	public Korisnik (int id) {
		this.id = id;
	}
	
	public Korisnik (int id, String ime, String prezime, Uloga uloga, double stanjeRacuna, String korisnickoIme,
			String lozinka, int odobren) {
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.uloga = uloga;
		this.stanjeRacuna = stanjeRacuna;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.odobren = odobren;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public double getStanjeRacuna() {
		return stanjeRacuna;
	}

	public void setStanjeRacuna(double stanjeRacuna) {
		this.stanjeRacuna = stanjeRacuna;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public int getOdobren() {
		return odobren;
	}

	public void setOdobren(int odobren) {
		this.odobren = odobren;
	}
	
	@Override
	public String toString() {
		return ime + " " + prezime;
	}
	
}
