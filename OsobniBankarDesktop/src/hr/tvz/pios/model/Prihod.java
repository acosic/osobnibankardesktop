package hr.tvz.pios.model;

import java.text.DecimalFormat;
import java.util.Date;

public class Prihod {
	
	private int id;
	private Korisnik korisnik;
	private Valuta valuta;
	private TipPrihoda tip;
	private double brutoIznos;
	private double netoIznos;
	private Date datum;
	
	public Prihod(int id, Korisnik korisnik, Valuta valuta, TipPrihoda tip, double brutoIznos, double netoIznos,
			Date datum) {

		this.id = id;
		this.korisnik = korisnik;
		this.valuta = valuta;
		this.tip = tip;
		this.brutoIznos = brutoIznos;
		this.netoIznos = netoIznos;
		this.datum = datum;
	}
	
	public Prihod(int id, Korisnik korisnik, Valuta valuta, TipPrihoda tip, double brutoIznos, Date datum) {

		this.id = id;
		this.korisnik = korisnik;
		this.valuta = valuta;
		this.tip = tip;
		this.brutoIznos = brutoIznos;
		this.datum = datum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Valuta getValuta() {
		return valuta;
	}

	public void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}

	public TipPrihoda getTip() {
		return tip;
	}

	public void setTip(TipPrihoda tip) {
		this.tip = tip;
	}

	public double getBrutoIznos() {
		return brutoIznos;
	}

	public void setBrutoIznos(double brutoIznos) {
		this.brutoIznos = brutoIznos;
	}

	public double getNetoIznos() {
		return netoIznos;
	}
	
	public String getNetoIznosIspis() {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(2);
		return df.format(netoIznos);
	}

	public void setNetoIznos(double netoIznos) {
		this.netoIznos = netoIznos;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}
	
	
}
