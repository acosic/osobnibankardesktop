package hr.tvz.pios.model;

public class Valuta {
	
	private String valuta;
	private int id;

	public Valuta(int id, String valuta) {
		this.id = id;
		this.valuta = valuta;
	}

	public String getValuta() {
		return valuta;
	}

	public void setValuta(String valuta) {
		this.valuta = valuta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return valuta;
	}
}
