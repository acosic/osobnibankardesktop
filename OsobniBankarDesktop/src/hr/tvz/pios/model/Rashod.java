package hr.tvz.pios.model;

import java.text.DecimalFormat;
import java.util.Date;

public class Rashod {

	private int id;
	private Korisnik korisnik;
	private double iznos;
	private Valuta valuta;
	private TipRashoda tip;
	private Date datum;
	private boolean planiraniRashod;
	
	
	public Rashod(int id, Korisnik korisnik, double iznos, Valuta valuta, TipRashoda tip, Date datum, 
			boolean planiraniRashod) {
		
		this.id = id;
		this.korisnik = korisnik;
		this.iznos = iznos;
		this.valuta = valuta;
		this.tip = tip;
		this.datum = datum;
		this.planiraniRashod = planiraniRashod;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public double getIznos() {
		return iznos;
	}


	public void setIznos(double iznos) {
		this.iznos = iznos;
	}


	public Valuta getValuta() {
		return valuta;
	}


	public void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}


	public TipRashoda getTip() {
		return tip;
	}


	public void setTip(TipRashoda tip) {
		this.tip = tip;
	}


	public Date getDatum() {
		return datum;
	}


	public void setDatum(Date datum) {
		this.datum = datum;
	}
	
	public String getIznosIspis() {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(2);
		return df.format(iznos);
	}


	public boolean isPlaniraniRashod() {
		return planiraniRashod;
	}


	public void setPlaniraniRashod(boolean planiraniRashod) {
		this.planiraniRashod = planiraniRashod;
	}


	public boolean getPlaniraniRashod() {
		return planiraniRashod;
	}
	
	public String getPlaniraniRashodTekst() {
		return planiraniRashod ? "Da" : "Ne";
	}
	
	
}