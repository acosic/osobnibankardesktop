package hr.tvz.pios.model;

public class TipRashoda {

	private int id;
	private String opis;
	
	public TipRashoda(int id, String opis) {
		this.id = id;
		this.opis = opis;
	}

	public String getOpis() {
		return opis;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return opis;
	}
}
