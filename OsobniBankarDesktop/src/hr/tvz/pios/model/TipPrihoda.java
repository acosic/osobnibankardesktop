package hr.tvz.pios.model;

public class TipPrihoda {
	
	private String opis;
	private int id;

	public TipPrihoda(int id, String opis) {
		this.id = id;
		this.opis = opis;
	}

	public String getOpis() {
		return opis;
	}
	
	public int getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return opis;
	}
}
