package hr.tvz.pios.kontroler;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Uloga;
import hr.tvz.pios.model.Valuta;
import hr.tvz.pios.poslovnalogika.Logika;

public class Kontroler {
	
	private Korisnik korisnik = null;
	private Logika logika = new Logika();
	private MessageDigest md;
	
	public Kontroler () {
		 try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	private String izracunajMD(String lozinka) {
		byte[] lozinkaMd5 = md.digest(lozinka.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < lozinkaMd5.length; i++) {
			sb.append(Integer.toString((lozinkaMd5[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
	
	public void dodajPrihod(Valuta valuta, TipPrihoda tip, double brutoIznos, Date datum) {
		Prihod prihod = new Prihod (-1, korisnik, valuta, tip, brutoIznos, datum);
		logika.dodajNoviPrihod(prihod);
	}
	
	public boolean registrirajKorisnika(String ime, String prezime, String korisnickoIme, String lozinka, Uloga uloga) {
		Korisnik korisnik = new Korisnik(-1, ime, prezime, uloga, 0.0, korisnickoIme, izracunajMD(lozinka), 0);
		return logika.spremiKorisnika(korisnik);
	}
	
	public void odobriKorisnika(Korisnik korisnik) {
		logika.odobriKorisnika(korisnik);
	}
	
	public List<Korisnik> dohvatiSveNeodobrene() {
		return logika.dohvatiSveNeodobrene();
	}
	
	public void zatvoriBazu() {
		logika.zatvoriBazu();
	}
	
	public boolean prijaviKorisnika(String korisnickoIme, String lozinka) {
		Korisnik korisnik = logika.dohvatiKorisnika(korisnickoIme);
		if (korisnik == null || korisnik.getOdobren() != 1 || (!(korisnik.getLozinka().equals(izracunajMD(lozinka))))) {
			return false;
		}
		this.korisnik = korisnik;
		return true;
	}

	public List<Valuta> dohvatiSveValute() {
		return logika.dohvatiSveValute();
	}
	
	public List<TipPrihoda> dohvatiSveTipovePrihoda() {
		return logika.dohvatiSveTipovePrihoda();
	}
	
	public List<TipRashoda> dohvatiSveTipoveRashoda() {
		return logika.dohvatiSveTipoveRashoda();
	}
	
	public void dodajRashod (Valuta valuta, TipRashoda tip, double iznos, Date datum, boolean planiraniRashod) {
		Rashod rashod = new Rashod (-1, korisnik, iznos, valuta, tip, datum, planiraniRashod);
		logika.dodajNoviRashod(rashod);
	}

	public List<Prihod> dohvatiSvePrihode() {
		return logika.dohvatiSvePrihode(korisnik);
	}
	
	public List<Rashod> dohvatiSveRashode() {
		return logika.dohvatiSveRashode(korisnik);
	}
	
	public double dohvatiStanjeRacuna() {
		return logika.dohvatiStanjeRacuna(korisnik);
	}
	
	public void azurirajPrihod(Prihod prihod) {
		logika.azurirajPrihod(prihod);
	}
	
	public void azurirajRashod(Rashod rashod) {
		logika.azurirajRashod(rashod);
	}
	
	public void generirajIzvjestaj(String datoteka, Date datumOd, Date datumDo) {
		logika.generirajIzvjestaj(datoteka, datumOd, datumDo, korisnik);
	}
	
	public boolean isAdmin() {
		return korisnik.getUloga().getNaziv().equals("Admin");
	}

	public String getKorisnikIme() {
		return korisnik.getIme() + " " + korisnik.getPrezime();
	}
}
