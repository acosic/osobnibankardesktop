package hr.tvz.pios.gui;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Valuta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.converter.LocalDateStringConverter;

/**
 * @author Tina
 *
 */
public class GlavniProzorKontroler implements Initializable {
	
	Kontroler kontroler;
	
	@FXML
	private Button btnKlik;
	@FXML
	private TextField tfStanjeRacuna;
	@FXML
	private Label lblImeKorisnika;
	@FXML
	private TableView<Prihod> tableTransakcije;
	@FXML
	private TableView<Rashod> tableTransakcije2;
	@FXML
	private TextField tfPrihodBrutoIznos;
	@FXML
	private TextField tfRashodIznos;
	@FXML
	private ComboBox<TipPrihoda> cbTipPrihoda;
	@FXML
	private ComboBox<Valuta> cbPrihodValuta;
	@FXML
	private ComboBox<TipRashoda> cbTipRashoda;
	@FXML
	private ComboBox<Valuta> cbRashodValuta;
	@FXML
	private DatePicker dpPrihodDatum;
	@FXML
	private DatePicker dpRashodDatum;
	@FXML
	private TabPane tabPane;
	@FXML
	private Tab tabLista;
	@FXML
	private Tab tabPrihod;
	@FXML
	private Tab tabRashod;
	@FXML
	private Tab tabAdministracija;
	@FXML
	private RadioButton checkPrihodi;
	@FXML
	private RadioButton checkRashodi;
	@FXML
	private CheckBox checkPlanirani;
	@FXML
	private DatePicker dpDatumOd;
	@FXML
	private DatePicker dpDatumDo;
	@FXML
	private ListView<Korisnik> neodobreniKorisnici;
	@FXML
	Parent root;
	
	private Prihod azuriraniPrihod = null;
	private Rashod azuriraniRashod = null;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

	}
	
	public void spremiNoviPrihod(ActionEvent event) {
		if (azuriraniPrihod != null) {
			azuriraniPrihod.setBrutoIznos(Double.parseDouble(tfPrihodBrutoIznos.getText()));
			azuriraniPrihod.setTip( cbTipPrihoda.getValue());
			azuriraniPrihod.setValuta(cbPrihodValuta.getValue());
			Instant instant = Instant.from(dpPrihodDatum.getValue().atStartOfDay(ZoneId.systemDefault()));
			azuriraniPrihod.setDatum(Date.from(instant));
			kontroler.azurirajPrihod(azuriraniPrihod);
			
			azuriraniPrihod = null;
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Prihod uspješno ažuriran.");

			alert.showAndWait();
			
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			tfStanjeRacuna.setText(String.valueOf(df.format(kontroler.dohvatiStanjeRacuna())));
			
			return;
		}
		
		try {
			Instant instant = Instant.from(dpPrihodDatum.getValue().atStartOfDay(ZoneId.systemDefault()));
			kontroler.dodajPrihod(cbPrihodValuta.getValue(), cbTipPrihoda.getValue(), Double.parseDouble(tfPrihodBrutoIznos.getText()),
						Date.from(instant));
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Prihod uspješno dodan.");

			alert.showAndWait();
			
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			tfStanjeRacuna.setText(String.valueOf(df.format(kontroler.dohvatiStanjeRacuna())));		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Pogrešno upisan bruto iznos");

			alert.showAndWait();
		}
	}
	
	public void spremiNoviRashod(ActionEvent event) {
		if (azuriraniRashod != null) {
			azuriraniRashod.setIznos(Double.parseDouble(tfRashodIznos.getText()));
			azuriraniRashod.setTip( cbTipRashoda.getValue());
			azuriraniRashod.setValuta(cbRashodValuta.getValue());
			Instant instant = Instant.from(dpRashodDatum.getValue().atStartOfDay(ZoneId.systemDefault()));
			azuriraniRashod.setDatum(Date.from(instant));
			azuriraniRashod.setPlaniraniRashod(checkPlanirani.isSelected());
			kontroler.azurirajRashod(azuriraniRashod);
			
			azuriraniRashod = null;
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Rashod uspješno ažuriran.");

			alert.showAndWait();
			
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			tfStanjeRacuna.setText(String.valueOf(df.format(kontroler.dohvatiStanjeRacuna())));			
			return;
		}
		
		try {
			boolean planirani = false;
			if (checkPlanirani.isSelected()) {
				planirani = true;
			}
			Instant instant = Instant.from(dpRashodDatum.getValue().atStartOfDay(ZoneId.systemDefault()));
			kontroler.dodajRashod(cbRashodValuta.getValue(), cbTipRashoda.getValue(), Double.parseDouble(tfRashodIznos.getText()),
						Date.from(instant), planirani);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Rashod uspješno dodan.");

			alert.showAndWait();
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			tfStanjeRacuna.setText(String.valueOf(df.format(kontroler.dohvatiStanjeRacuna())));		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Pogrešno upisan iznos");

			alert.showAndWait();
		}
	}
	
	public void prikaziPrihode(ActionEvent event) {
		final ObservableList<Prihod> listaPrihoda = FXCollections.observableArrayList(kontroler.dohvatiSvePrihode());
		tableTransakcije.setItems(listaPrihoda);
		tableTransakcije.setVisible(true);
		tableTransakcije2.setVisible(false);
	}
	
	public void prikaziRashode(ActionEvent event) {
		final ObservableList<Rashod> listaRashoda = FXCollections.observableArrayList(kontroler.dohvatiSveRashode());
		tableTransakcije2.setItems(listaRashoda);
		tableTransakcije2.setVisible(true);
		tableTransakcije.setVisible(false);
	}

	public void ocistiPrihodRashod(ActionEvent event) {
        cbTipPrihoda.setValue(kontroler.dohvatiSveTipovePrihoda().get(0));
        cbPrihodValuta.setValue(kontroler.dohvatiSveValute().get(0));
        dpPrihodDatum.setValue(LocalDate.now());
        tfPrihodBrutoIznos.setText("");
        tfRashodIznos.setText("");
        cbTipRashoda.setValue(kontroler.dohvatiSveTipoveRashoda().get(0));
        cbRashodValuta.setValue(kontroler.dohvatiSveValute().get(0));
        checkPlanirani.setSelected(false);
		azuriraniPrihod = null;
		azuriraniRashod = null;
	}
	
	public void generirajIzvjestaj(ActionEvent event) {
		Instant instantOd = Instant.from(dpDatumOd.getValue().atStartOfDay(ZoneId.systemDefault()));
		Instant instantDo = Instant.from(dpDatumDo.getValue().plusDays(1).atStartOfDay(ZoneId.systemDefault()));
		
		FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Generiraj izvještaj");
        File file = fileChooser.showSaveDialog(root.getScene().getWindow());
        if (file != null) {
            kontroler.generirajIzvjestaj(file.getAbsolutePath(), Date.from(instantOd), Date.from(instantDo));
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Izvještaj uspjesno generiran.");
			alert.showAndWait();
        }
	}

	public void setKontroler(Kontroler kontroler) {
		this.kontroler = kontroler;
		final ObservableList<Prihod> listaPrihoda = FXCollections.observableArrayList(kontroler.dohvatiSvePrihode());
		final ObservableList<TipPrihoda> tipoviPrihoda = FXCollections.observableArrayList(kontroler.dohvatiSveTipovePrihoda());
		final ObservableList<Valuta> valute = FXCollections.observableArrayList(kontroler.dohvatiSveValute());
		final ObservableList<TipRashoda> tipoviRashoda = FXCollections.observableArrayList(kontroler.dohvatiSveTipoveRashoda());

		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(2);
		tfStanjeRacuna.setText(String.valueOf(df.format(kontroler.dohvatiStanjeRacuna())));		lblImeKorisnika.setText("Prijavljeni ste kao " + kontroler.getKorisnikIme());
		
		TableColumn<Prihod, Date> datumColumn = new TableColumn<Prihod, Date>("Datum");
		datumColumn.setCellValueFactory(new PropertyValueFactory<>("datum"));
        TableColumn<Prihod, Double> iznosColumn = new TableColumn<Prihod, Double>("Iznos");
        iznosColumn.setCellValueFactory(new PropertyValueFactory<>("netoIznosIspis"));
        TableColumn valutaColumn = new TableColumn("Valuta");
        valutaColumn.setCellValueFactory(new PropertyValueFactory<>("valuta"));
        TableColumn tipColumn = new TableColumn("Tip prihoda");
        tipColumn.setCellValueFactory(new PropertyValueFactory<>("tip"));
        tableTransakcije.getColumns().addAll(datumColumn, iznosColumn, valutaColumn, tipColumn);
        tableTransakcije.setItems(listaPrihoda);
        
		TableColumn<Rashod, Date> datumColumn2 = new TableColumn<Rashod, Date>("Datum");
		datumColumn2.setCellValueFactory(new PropertyValueFactory<>("datum"));
        TableColumn<Rashod, Double> iznosColumn2 = new TableColumn<Rashod, Double>("Iznos");
        iznosColumn2.setCellValueFactory(new PropertyValueFactory<>("iznosIspis"));
        TableColumn valutaColumn2 = new TableColumn("Valuta");
        valutaColumn2.setCellValueFactory(new PropertyValueFactory<>("valuta"));
        TableColumn tipColumn2 = new TableColumn("Tip rashoda");
        tipColumn2.setCellValueFactory(new PropertyValueFactory<>("tip"));
        TableColumn<Rashod, String> planiraniColumn2 = new TableColumn<Rashod, String>("Planirani");
        planiraniColumn2.setCellValueFactory(new PropertyValueFactory<>("planiraniRashodTekst"));
        tableTransakcije2.getColumns().addAll(datumColumn2, iznosColumn2, valutaColumn2, tipColumn2, planiraniColumn2);
        
        tableTransakcije.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				MouseEvent me = (MouseEvent)event;
				if (me.getClickCount() == 2) {
					Prihod prihod = tableTransakcije.getSelectionModel().getSelectedItem();
					if (prihod == null) {
						return;
					}
					tabPane.getSelectionModel().select(tabPrihod);
					tfPrihodBrutoIznos.setText(Double.toString(prihod.getBrutoIznos()));
					cbPrihodValuta.setValue(prihod.getValuta());
					cbTipPrihoda.setValue(prihod.getTip());
					Instant instant = Instant.ofEpochMilli(prihod.getDatum().getTime());
					LocalDate ld = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
					dpPrihodDatum.setValue(ld);
					azuriraniPrihod = prihod;
				}
			}
		});
        
        tableTransakcije2.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				MouseEvent me = (MouseEvent)event;
				if (me.getClickCount() == 2) {
					Rashod rashod = tableTransakcije2.getSelectionModel().getSelectedItem();
					if (rashod == null) {
						return;
					}
					tabPane.getSelectionModel().select(tabRashod);
					tfRashodIznos.setText(Double.toString(rashod.getIznos()));
					cbRashodValuta.setValue(rashod.getValuta());
					cbTipRashoda.setValue(rashod.getTip());
					Instant instant = Instant.ofEpochMilli(rashod.getDatum().getTime());
					LocalDate ld = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
					dpRashodDatum.setValue(ld);
					checkPlanirani.setSelected(rashod.getPlaniraniRashod());
			        azuriraniRashod = rashod;
				}
			}
		});
        
        cbTipPrihoda.setItems(tipoviPrihoda);
        cbPrihodValuta.setItems(valute);
        cbTipRashoda.setItems(tipoviRashoda);
        cbRashodValuta.setItems(valute);
        
        cbTipPrihoda.setValue(kontroler.dohvatiSveTipovePrihoda().get(0));
        cbPrihodValuta.setValue(kontroler.dohvatiSveValute().get(0));
        cbTipRashoda.setValue(kontroler.dohvatiSveTipoveRashoda().get(0));
        cbRashodValuta.setValue(kontroler.dohvatiSveValute().get(0));
        dpPrihodDatum.setValue(LocalDate.now());
        dpRashodDatum.setValue(LocalDate.now());
        
        tabLista.setOnSelectionChanged(new EventHandler<Event>() {
			
			@Override
			public void handle(Event event) {
				final ObservableList<Prihod> listaPrihoda = FXCollections.observableArrayList(kontroler.dohvatiSvePrihode());
				tableTransakcije.setItems(listaPrihoda);
				tableTransakcije.setVisible(true);
				tableTransakcije2.setVisible(false);
				checkPrihodi.setSelected(true);
				checkRashodi.setSelected(false);
				azuriraniPrihod = null;
		        azuriraniRashod = null;
			}
		});
        tabPrihod.setOnSelectionChanged(new EventHandler<Event>() {
			
			@Override
			public void handle(Event event) {
		        cbTipPrihoda.setValue(kontroler.dohvatiSveTipovePrihoda().get(0));
		        cbPrihodValuta.setValue(kontroler.dohvatiSveValute().get(0));
		        dpPrihodDatum.setValue(LocalDate.now());
		        tfPrihodBrutoIznos.setText("");
		        tfRashodIznos.setText("");
		        cbTipRashoda.setValue(kontroler.dohvatiSveTipoveRashoda().get(0));
		        cbRashodValuta.setValue(kontroler.dohvatiSveValute().get(0));
		        checkPlanirani.setSelected(false);
		        azuriraniPrihod = null;
		        azuriraniRashod = null;
			}
		});
        tabRashod.setOnSelectionChanged(new EventHandler<Event>() {
			
			@Override
			public void handle(Event event) {
		        cbTipPrihoda.setValue(kontroler.dohvatiSveTipovePrihoda().get(0));
		        cbPrihodValuta.setValue(kontroler.dohvatiSveValute().get(0));
		        dpPrihodDatum.setValue(LocalDate.now());
		        tfPrihodBrutoIznos.setText("");
		        tfRashodIznos.setText("");
		        cbTipRashoda.setValue(kontroler.dohvatiSveTipoveRashoda().get(0));
		        cbRashodValuta.setValue(kontroler.dohvatiSveValute().get(0));
		        azuriraniPrihod = null;
		        checkPlanirani.setSelected(false);
		        azuriraniRashod = null;
			}
		});
        
        if (!kontroler.isAdmin()) {
        	tabAdministracija.setDisable(true);
        } else {
    		final ObservableList<Korisnik> listaNeodobrenih = FXCollections.observableArrayList(kontroler.dohvatiSveNeodobrene());
        	neodobreniKorisnici.setItems(listaNeodobrenih);
        }
	}
	
	public void odjaviKorisnika(ActionEvent event) {
		try {                    
			URL url = getClass().getResource("LoginProzor.fxml");
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(url);
			fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
			root = (Parent)fxmlLoader.load(url.openStream());            
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			kontroler.zatvoriBazu();
			stage.show();
			((Node)(event.getSource())).getScene().getWindow().hide();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void odobriKorisnika(ActionEvent event) {
		Korisnik k = neodobreniKorisnici.getSelectionModel().getSelectedItem();
		if (k != null) {
			kontroler.odobriKorisnika(k);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Korisnik odobren.");
			alert.showAndWait();
			final ObservableList<Korisnik> listaNeodobrenih = FXCollections.observableArrayList(kontroler.dohvatiSveNeodobrene());
        	neodobreniKorisnici.setItems(listaNeodobrenih);
		}
	}
	
	public void odobriSveKorisnike(ActionEvent event) {
		List<Korisnik> sviNeodobreni = kontroler.dohvatiSveNeodobrene();
		for (int i = 0; i < sviNeodobreni.size(); i++) {
			kontroler.odobriKorisnika(sviNeodobreni.get(i));
		}
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Uspješno");
		alert.setHeaderText("Korisnici odobreni.");
		alert.showAndWait();
		final ObservableList<Korisnik> listaNeodobrenih = FXCollections.observableArrayList(kontroler.dohvatiSveNeodobrene());
    	neodobreniKorisnici.setItems(listaNeodobrenih);
	}
}
