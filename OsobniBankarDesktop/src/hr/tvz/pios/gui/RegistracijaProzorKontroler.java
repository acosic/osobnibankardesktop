package hr.tvz.pios.gui;

import java.net.URL;
import java.util.ResourceBundle;

import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Uloga;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;

public class RegistracijaProzorKontroler implements Initializable {

	Kontroler kontroler;

	@FXML
	private Parent root;
	@FXML
	private TextField tfIme;
	@FXML
	private TextField tfPrezime;
	@FXML
	private TextField tfKorisnickoIme;
	@FXML
	private PasswordField tfLozinka;
	@FXML
	private PasswordField tfPonovljenaLozinka;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		kontroler = new Kontroler();
	}
	
	public void registriraj(ActionEvent event) {
		if (!tfLozinka.getText().equals(tfPonovljenaLozinka.getText())) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Lozinke se ne poklapaju.");
			alert.showAndWait();
			return;
		}
		if (tfLozinka.getText().equals("") || tfKorisnickoIme.getText().equals("")) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Pogrešno upisani podaci.");
			alert.showAndWait();
			return;
		}
		boolean uspjesno = kontroler.registrirajKorisnika(tfIme.getText(), tfPrezime.getText(), tfKorisnickoIme.getText(),
				tfLozinka.getText(), new Uloga(1, "Korisnik"));
		if (!uspjesno) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Korisničko ime već postoji.");
			alert.showAndWait();
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Uspješno");
			alert.setHeaderText("Uspješno ste se registrirali. Pričekajte da vas adminstartor odobri.");
			alert.showAndWait();
			kontroler.zatvoriBazu();
			root.getScene().getWindow().hide();
		}
	}
}
