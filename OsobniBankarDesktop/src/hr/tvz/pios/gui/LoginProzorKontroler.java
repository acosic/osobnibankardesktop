package hr.tvz.pios.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import hr.tvz.pios.kontroler.Kontroler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;

public class LoginProzorKontroler implements Initializable {

	Kontroler kontroler;
	
	@FXML
	private Label lblGreska;
	@FXML
	private TextField tfKorisnik;
	@FXML
	private PasswordField tfLozinka;
	@FXML
	private Parent root;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lblGreska.setText("");
	}
	
	public void prijaviKorisnika(ActionEvent event) {
		kontroler = new Kontroler();
		
		if (tfKorisnik.getText().equals("") || tfLozinka.getText().equals("")) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Greška");
			alert.setHeaderText("Morate unijeti korisničko ime i lozinku.");
			alert.showAndWait();
		}
		
		boolean uspjesno = kontroler.prijaviKorisnika(tfKorisnik.getText(), tfLozinka.getText());
		if (!uspjesno) {
			lblGreska.setText("Pogrešno uneseni podaci ili još niste odobreni.");
			return;
		}
		
		 try {                    
		    URL url = getClass().getResource("GlavniProzor.fxml");
		    FXMLLoader fxmlLoader = new FXMLLoader();
		    fxmlLoader.setLocation(url);
		    fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
		    root = (Parent)fxmlLoader.load(url.openStream());            
		    Stage stage = new Stage();
		    stage.setScene(new Scene(root));
		    stage.setTitle("Osobni bankar");
		    GlavniProzorKontroler glavniProzorKontroler = fxmlLoader.<GlavniProzorKontroler>getController();
		    glavniProzorKontroler.setKontroler(kontroler);
		    stage.show();
		    ((Node)(event.getSource())).getScene().getWindow().hide();
		  } catch (IOException e) {
		      e.printStackTrace();
		  }
	}
	
	public void registriraj(ActionEvent event) {
		 try {                    
		    URL url = getClass().getResource("RegistracijaProzor.fxml");
		    FXMLLoader fxmlLoader = new FXMLLoader();
		    fxmlLoader.setLocation(url);
		    fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
		    root = (Parent)fxmlLoader.load(url.openStream());            
		    Stage stage = new Stage();
		    stage.setScene(new Scene(root));
		    stage.setTitle("Registracija");
		    stage.show();
		  } catch (IOException e) {
		      e.printStackTrace();
		  }
	}
}
