package hr.tvz.pios.poslovnalogika;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hr.tvz.pios.bazapodataka.Baza;
import hr.tvz.pios.izvjestaj.Izvjestaj;
import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Valuta;
import hr.tvz.pios.webservis.Servis;
import net.sf.jasperreports.engine.JRException;

public class Logika {

	private Baza baza = new Baza();
	private Servis servis = new Servis();

	public void dodajNoviPrihod(Prihod prihod) {
		prihod.setNetoIznos(prihod.getBrutoIznos() * 0.6);
		baza.dodajPrihod(prihod);
		
		Korisnik korisnik = prihod.getKorisnik();
		double iznos = dohvatiStanjeRacuna(korisnik) + servis.preracunajUKune(prihod.getValuta(), prihod.getNetoIznos(), prihod.getDatum());
		korisnik.setStanjeRacuna(iznos);
		baza.azurirajStanjeRacuna(korisnik);
		
	}

	public List<Valuta> dohvatiSveValute() {
		List<Valuta> valute = new ArrayList<Valuta>();
		
		Valuta defaultnaValuta = baza.dohvatiDefaultnuValutu();
		valute.add(defaultnaValuta);
		valute.addAll(baza.dohvatiPreostaleValute());
		return valute;
	}

	public List<TipPrihoda> dohvatiSveTipovePrihoda() {
		return baza.dohvatiSveTipovePrihoda();
	}

	public List<TipRashoda> dohvatiSveTipoveRashoda() {
		return baza.dohvatiSveTipoveRashoda();
	}

	public void dodajNoviRashod(Rashod rashod) {
		baza.dodajRashod(rashod);
		
		Korisnik korisnik = rashod.getKorisnik();
		double iznos = dohvatiStanjeRacuna(korisnik) - servis.preracunajUKune(rashod.getValuta(), rashod.getIznos(), rashod.getDatum());
		korisnik.setStanjeRacuna(iznos);
		baza.azurirajStanjeRacuna(korisnik);
		
	}

	public List<Prihod> dohvatiSvePrihode(Korisnik korisnik) {
		return baza.dohvatiSvePrihode(korisnik);
	}

	public List<Rashod> dohvatiSveRashode(Korisnik korisnik) {
		return baza.dohvatiSveRashode(korisnik);
	}

	public double dohvatiStanjeRacuna(Korisnik korisnik) {
		return baza.dohvatiStanjeRacuna(korisnik);
	}

	public void azurirajPrihod(Prihod prihod) {
		Prihod zadnjiPrihod = baza.dohvatiPrihod(prihod.getKorisnik(), prihod.getId());		
		
		prihod.setNetoIznos(prihod.getBrutoIznos() * 0.6);
		baza.azurirajPrihod(prihod);

		Korisnik korisnik = prihod.getKorisnik();
		double iznos = dohvatiStanjeRacuna(korisnik) + servis.preracunajUKune(prihod.getValuta(), prihod.getNetoIznos(), prihod.getDatum()) - 
				servis.preracunajUKune(zadnjiPrihod.getValuta(), zadnjiPrihod.getNetoIznos(), zadnjiPrihod.getDatum());
		korisnik.setStanjeRacuna(iznos);
		baza.azurirajStanjeRacuna(korisnik);
	}

	public void azurirajRashod(Rashod rashod) {
		Rashod zadnjiRashod = baza.dohvatiRashod(rashod.getKorisnik(), rashod.getId());	
		baza.azurirajRashod(rashod);
		
		Korisnik korisnik = rashod.getKorisnik();
		double iznos = dohvatiStanjeRacuna(korisnik) - servis.preracunajUKune(rashod.getValuta(), rashod.getIznos(), rashod.getDatum()) + 
				servis.preracunajUKune(zadnjiRashod.getValuta(), zadnjiRashod.getIznos(), zadnjiRashod.getDatum());
		korisnik.setStanjeRacuna(iznos);
		baza.azurirajStanjeRacuna(korisnik);
	}

	public void generirajIzvjestaj(String datoteka, Date datumOd, Date datumDo, Korisnik korisnik) {
		try {
			Izvjestaj.stvoriIzvjestaj(baza.dohvatiKonekciju(), datoteka, datumOd, datumDo, korisnik);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Korisnik dohvatiKorisnika(String korisnickoIme) {
		return baza.dohvatiKorisnika(korisnickoIme);
	}

	public boolean spremiKorisnika(Korisnik korisnik) {
		Korisnik k = baza.dohvatiKorisnika(korisnik.getKorisnickoIme());
		if (k == null) {
			baza.spremiKorisnika(korisnik);
			return true;
		} else {
			return false;
		}

	}

	public void odobriKorisnika(Korisnik korisnik) {
		baza.odobriKorisnika(korisnik);
		
	}

	public List<Korisnik> dohvatiSveNeodobrene() {
		return baza.dohvatiSveNeodobrene();
	}

	public void zatvoriBazu() {
		baza.zatvoriBazu();		
	}
}
