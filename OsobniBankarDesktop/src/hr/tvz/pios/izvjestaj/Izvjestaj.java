package hr.tvz.pios.izvjestaj;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hr.tvz.pios.model.Korisnik;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRProperties;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class Izvjestaj {
	
	public static void stvoriIzvjestaj(Connection conn, String datoteka, Date datumOd, Date datumDo, 
			Korisnik korisnik) throws JRException {
		InputStream input = null;
		try {
			input = new FileInputStream(new File("first-report.jrxml"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		JasperDesign jasperDesign = JRXmlLoader.load(input);
		JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("StanjeRacuna", new String(String.format("%.2f", korisnik.getStanjeRacuna())));
		parameters.put("DatumOd", new Long(datumOd.getTime()));
		System.out.println(datumOd.getTime());
		parameters.put("DatumDo", new Long(datumDo.getTime()));
		System.out.println(datumDo.getTime());
		parameters.put("KorisnikId", new Integer(korisnik.getId()));
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);
		
		OutputStream output = null;
		try {
			output = new FileOutputStream(new File(datoteka + ".pdf"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		JasperExportManager.exportReportToPdfStream(jasperPrint, output);
		try {
			output.close();
			input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
