package hr.tvz.pios.webservis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.print.attribute.standard.DateTimeAtCompleted;

import hr.tvz.pios.gui.GlavniProzorKontroler;
import hr.tvz.pios.model.Valuta;

public class Servis {
	
	private List<Tecaj> listaTecaja = null;
			
	
		
	public static ArrayList<Tecaj> dohvatiTecajeve(Date datum){
		
		ArrayList<Tecaj> listaTecaja = new ArrayList<>();
		SimpleDateFormat formatDatuma = new SimpleDateFormat("ddMMyy");
		InputStream in = null;
		try {
			String odabraniDatumString = formatDatuma.format(datum);
			URL u =  new URL ("http://hnb.hr/tecajn/f" + odabraniDatumString + ".dat");
			try{
				in = u.openStream();
			}catch(FileNotFoundException ex) {
				System.out.print("Provjerite internet vezu.");
				System.exit(1);
			}
			BufferedReader reader = new BufferedReader (new InputStreamReader(in));
			String line = reader.readLine();
			while ((line = reader.readLine()) != null){
				String valuta = line.substring(3, 6);
				String jedinica = line.substring(6, 9);
				StringTokenizer tokenizer = new StringTokenizer(line, " ");
				tokenizer.nextToken();
				tokenizer.nextToken();
				String tecaj = tokenizer.nextToken().replaceFirst(",", ".");
				double tecajPremaKuni= Double.parseDouble(tecaj);
				tecajPremaKuni = (tecajPremaKuni/(Double.parseDouble(jedinica)));
				Tecaj novo = new Tecaj(valuta, tecajPremaKuni);
				listaTecaja.add(novo);
			}
		} catch (IOException ex){
			System.err.println("Došlo je do pogreške prilikom dohvata tečaja!");
			System.exit(1);
				
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
			}
		}
		listaTecaja.add(new Tecaj("HRK", 1));
		return listaTecaja;
	}
	
	public double preracunajUKune(Valuta valuta, double iznos, Date datum) {
		double iznosUKunama;
		listaTecaja=Servis.dohvatiTecajeve(datum);
		for (int i = 0; i < listaTecaja.size(); i++) {
			if (valuta.getValuta().equals(listaTecaja.get(i).getNaziv())) {
				iznosUKunama = iznos * listaTecaja.get(i).getTecajPremaKuni();
				return iznosUKunama;
			}
		}
		throw new RuntimeException("Nepostojeća valuta.");
	}

	



}
