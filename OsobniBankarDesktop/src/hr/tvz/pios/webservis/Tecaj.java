package hr.tvz.pios.webservis;

import java.util.Date;

import hr.tvz.pios.model.Valuta;

public class Tecaj {
	
	private String naziv;
	private double tecajPremaKuni;
	private Date datum;
	private Valuta valuta;
	
	public Valuta getValuta() {
		return valuta;
	}

	public void setValuta(Valuta valuta) {
		this.valuta = valuta;
	}

	public Tecaj(String naziv, double tecajPremaKuni) {
		this.naziv = naziv;
		this.tecajPremaKuni = tecajPremaKuni;
	}
	
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getTecajPremaKuni() {
		return tecajPremaKuni;
	}
	public void setTecajPremaKuni(double tecajPremaKuni) {
		this.tecajPremaKuni = tecajPremaKuni;
	}
	
	

}
