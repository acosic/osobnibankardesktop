package hr.tvz.pios.test;

import java.util.Date;
import java.util.List;

import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Valuta;

public class TestStanja {

	public static void main(String[] args) {

		Kontroler kontroler = new Kontroler();
		
		List<Korisnik> korisnici = null;
		boolean uspjelo = kontroler.prijaviKorisnika("perica", "123");
		if (uspjelo == true) {
			System.out.println("USPJEŠNO");
		}
		
		//baza test dodavanja prihoda
		Valuta valute = kontroler.dohvatiSveValute().get(0);
		TipPrihoda tipoviPrihoda = kontroler.dohvatiSveTipovePrihoda().get(0);
		kontroler.dodajPrihod(valute, tipoviPrihoda, 1000, new Date());
		
		/*
		//citanje stanja racuna test
		System.out.println(kontroler.dohvatiStanjeRacuna());
		
		//baza test dodavanja rashoda
		valute = kontroler.dohvatiSveValute().get(1);
		TipRashoda tipoviRashoda = kontroler.dohvatiSveTipoveRashoda().get(0);
		kontroler.dodajRashod(valute, tipoviRashoda, 400, false);
		
		//citanje stanja racuna test
		System.out.println(kontroler.dohvatiStanjeRacuna());
		*/
	}

}
