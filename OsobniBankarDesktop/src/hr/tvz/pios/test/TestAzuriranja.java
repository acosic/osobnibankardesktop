package hr.tvz.pios.test;

import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;

public class TestAzuriranja {

	public static void main(String[] args) {

		Kontroler kontroler = new Kontroler();
		
		/*
		//baza test ažuriranja prihoda
		Prihod prihod = kontroler.dohvatiSvePrihode().get(14);
		System.out.println(prihod.getBrutoIznos());
		prihod.setBrutoIznos(2000);
		kontroler.azurirajPrihod(prihod);
		System.out.println(prihod.getBrutoIznos());
		
		//baza test ažuriranja rashoda
		Rashod rashod = kontroler.dohvatiSveRashode().get(9);
		System.out.println(rashod.getIznos());
		rashod.setIznos(2000);
		rashod.setPlaniraniRashod(false);
		kontroler.azurirajRashod(rashod);
		System.out.println(rashod.getIznos());
		
		
		//baza test ažuriranja prihoda + čitanje stanja
		System.out.println(kontroler.dohvatiStanjeRacuna());
		Prihod prihod = kontroler.dohvatiSvePrihode().get(14);
		System.out.println(prihod.getNetoIznos());
		prihod.setBrutoIznos(3000);
		kontroler.azurirajPrihod(prihod);
		System.out.println(prihod.getNetoIznos());
		System.out.println(kontroler.dohvatiStanjeRacuna());
		*/
		
		//baza test ažuriranja rashoda
		System.out.println(kontroler.dohvatiStanjeRacuna());
		Rashod rashod = kontroler.dohvatiSveRashode().get(9);
		System.out.println(rashod.getIznos());
		rashod.setIznos(900);
		kontroler.azurirajRashod(rashod);
		System.out.println(rashod.getIznos());
		System.out.println(kontroler.dohvatiStanjeRacuna());
	}
}
