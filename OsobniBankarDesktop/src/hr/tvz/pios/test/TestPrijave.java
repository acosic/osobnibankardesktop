package hr.tvz.pios.test;

import java.util.List;

import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Korisnik;
import hr.tvz.pios.model.Uloga;

public class TestPrijave {

public static void main(String[] args) {
		
		Kontroler kontroler = new Kontroler();
		List<Korisnik> korisnici = null;
		boolean uspjelo = kontroler.prijaviKorisnika("perica", "123");
		if (uspjelo == true) {
			System.out.println("USPJEŠNO");
		}
		
		kontroler.registrirajKorisnika("Tinki", "Vinki", "teletabis", "987", new Uloga(1,"Korisnik"));
		
		kontroler.registrirajKorisnika("a", "a", "a", "987", new Uloga(1,"Korisnik"));
		kontroler.registrirajKorisnika("b", "b", "b", "987", new Uloga(1,"Korisnik"));
		kontroler.registrirajKorisnika("c", "c", "c", "987", new Uloga(1,"Korisnik"));
		kontroler.registrirajKorisnika("e", "e", "e", "987", new Uloga(1,"Korisnik"));
		korisnici = kontroler.dohvatiSveNeodobrene();
		for (int i = 0; i < korisnici.size(); i++) {
			System.out.println(korisnici.get(i).getKorisnickoIme());
			System.out.println(korisnici.get(i).getOdobren());
			kontroler.odobriKorisnika(korisnici.get(i));
		}
		korisnici = kontroler.dohvatiSveNeodobrene();
		System.out.println(korisnici.size());
	}
}
