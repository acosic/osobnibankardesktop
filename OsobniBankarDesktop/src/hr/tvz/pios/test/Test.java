package hr.tvz.pios.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hr.tvz.pios.kontroler.Kontroler;
import hr.tvz.pios.model.Prihod;
import hr.tvz.pios.model.Rashod;
import hr.tvz.pios.model.TipPrihoda;
import hr.tvz.pios.model.TipRashoda;
import hr.tvz.pios.model.Valuta;

public class Test {
	
	public static void main(String[] args) {
		
		Kontroler kontroler = new Kontroler();
				
		//valute test
		List<Valuta> valute = kontroler.dohvatiSveValute();
		for (int i = 0; i < valute.size(); i++) {
			System.out.println(valute.get(i).getValuta());
		}
		
		//tipoviPrihoda test
		List<TipPrihoda> tipoviPrihoda = kontroler.dohvatiSveTipovePrihoda();
		for (int i = 0; i < tipoviPrihoda.size(); i++) {
			System.out.println(tipoviPrihoda.get(i).getOpis());
		}
		
		//baza test dodavanja prihoda
		kontroler.dodajPrihod(valute.get(1), tipoviPrihoda.get(2), 8888.7, new Date());
		
		//tipoviRashoda test
		List<TipRashoda> tipoviRashoda = kontroler.dohvatiSveTipoveRashoda();
		for (int i = 0; i < tipoviRashoda.size(); i++) {
			System.out.println(tipoviRashoda.get(i).getOpis());
		}
		
		//baza test dodavanja rashoda
		kontroler.dodajRashod(valute.get(0), tipoviRashoda.get(3), 150, new Date(), true);
		
		//citanje svih prihoda test
		List<Prihod> prihodi = kontroler.dohvatiSvePrihode();
		for (int i = 0; i < prihodi.size(); i++) {
			Prihod p = prihodi.get(i);
			System.out.println("Bruto: " + p.getBrutoIznos() + p.getValuta().getValuta() + ", neto: " + 
					p.getNetoIznos() + p.getValuta().getValuta() + " datum: " + p.getDatum());
		}
		
		//citanje svih rashoda test
		List<Rashod> rashodi = kontroler.dohvatiSveRashode();
		for (int i = 0; i < rashodi.size(); i++) {
			Rashod r = rashodi.get(i);
			System.out.println("Iznos: " + r.getIznos() + r.getValuta().getValuta() + " datum: " + r.getDatum());
		}
		
		//citanje stanja racuna test
		System.out.println(kontroler.dohvatiStanjeRacuna());
		
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		try {
			kontroler.generirajIzvjestaj("nesto.pdf", format.parse("20-04-2016"), format.parse("25-04-2016"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
