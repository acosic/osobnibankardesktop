package hr.tvz.pios.test;

import java.util.Date;

import hr.tvz.pios.model.Valuta;
import hr.tvz.pios.webservis.Servis;

public class TestKonverzije {
	
	public static void main(String[] args) {
		
		Servis servis = new Servis();
		double preracunato = servis.preracunajUKune(new Valuta(1, "EUR"), 100.00, 2016-03-06);
		System.out.println(preracunato);
		
	}

}
